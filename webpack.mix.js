const mix = require('laravel-mix');
const config = require('./webpack.config');
require('laravel-mix-svg-sprite');

mix
  .webpackConfig(config)
  .js('assets/src/js/app.js', 'js')
  .sass('assets/src/scss/app.scss', 'css')
  .setPublicPath('dist')
  .copyDirectory('assets/src/images', 'dist/images')
  .svgSprite('assets/src/svg', 'sprite.svg');

mix.babelConfig({
  presets: ['@babel/preset-env'],
});
