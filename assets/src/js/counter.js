function counter(element) {
  if (!element) {
    return;
  }

  const decrease = element.querySelector('[data-button="decrease"]');
  const increase = element.querySelector('[data-button="increase"]');
  const numberPlace = element.querySelector('[data-input]');
  let number = 0;
  const min = 0;
  const max = 1000;

  decrease.onclick = function () {
    if (number > min) {
      number -= 1;
      numberPlace.value = number;
    }
  };

  increase.onclick = function () {
    if (number < max) {
      number += 1;
      numberPlace.value = number;
    }
  };
}

const elementQuantity = document.querySelector('[data-quantity]');
counter(elementQuantity);
