import 'mmenu-js';
import 'mmenu-js/dist/mmenu.css';

document.addEventListener('DOMContentLoaded', () => {
  const menu = new window.Mmenu('#my-menu', {
    extensions: ['theme-white'],
    navbar: {
      title: 'Меню',
    },
    navbars: [
      {
        position: 'bottom',
        title: 'Меню',
        content: [
          '<a class="a-clear" href="tel:84959245585">+7 495 924-55-85</a>',
          '<a class="a-clear" href="tel:89859245585">+7 495 924-55-85</a>',
          '<a class="a-clear" href="mailto:info@skladslabov.ru">INFO@SKLADSLABOV.RU</a>',
        ],
      },
    ],
  });
});
