import Swiper, { Navigation, Pagination, Thumbs, Autoplay } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

Swiper.use([Navigation, Pagination, Thumbs, Autoplay]);

const mainSwiper = new Swiper('.main-swiper', {
  spaceBetween: 40,
  pagination: {
    el: '.swiper-pagination-main',
    clickable: true,
  },
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
});

const recommend = new Swiper('.recommend-swiper', {
  modules: [Pagination],
  slidesPerView: 2,
  spaceBetween: 20,
  pagination: {
    el: '.swiper-pagination-recommend',
    clickable: true,
  },
  breakpoints: {
    768: {
      slidesPerView: 3,
      spaceBetween: 34,
    },
  },
});

const productThumbs = new Swiper('.product__thumbs', {
  spaceBetween: 8,
  slidesPerView: 6,
  watchSlidesProgress: true,
  navigation: {
    nextEl: '.product__button_next',
    prevEl: '.product__button_prev',
  },
  breakpoints: {
    768: {
      slidesPerView: 3,
      spaceBetween: 10,
    },
    1400: {
      slidesPerView: 4,
      spaceBetween: 10,
    },
  },
});

const productMain = new Swiper('.product__main', {
  spaceBetween: 10,
  watchOverflow: true,
  navigation: {
    nextEl: '.product__button_next',
    prevEl: '.product__button_prev',
  },
  thumbs: {
    swiper: productThumbs,
  },
});

const related = new Swiper('.related-swiper', {
  modules: [Pagination],
  slidesPerView: 1,
  spaceBetween: 34,
  grid: {
    rows: 2,
  },
  pagination: {
    el: '.swiper-pagination-related',
    clickable: true,
  },
});

const question = new Swiper('.question-swiper', {
  modules: [Pagination],
  slidesPerView: 1,
  spaceBetween: 34,
  pagination: {
    el: '.swiper-pagination-question',
    clickable: true,
  },
  breakpoints: {
    1000: {
      slidesPerView: 1,
      spaceBetween: 30,
    },
  },
});

const extra = new Swiper('.main-extra-swiper', {
  modules: [Pagination],
  slidesPerView: 1,
  spaceBetween: 34,
  pagination: {
    el: '.swiper-pagination-extra',
    clickable: true,
  },
  breakpoints: {
    768: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
  },
});
