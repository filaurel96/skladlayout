function initInfrastructureMap() {
  const element = document.querySelector('.js-map');

  if (!element) {
    return;
  }

  const { latitude, longitude } = element.dataset;
  const map = new ymaps.Map(element, {
    center: [55.6159635924014, 37.483615133438754],
    zoom: 15,
  });

  const placemarkLogo = new ymaps.Placemark(
    [55.6159635924014, 37.483615133438754],
    {},
    {
      iconLayout: 'default#image',
    }
  );

  map.geoObjects.add(placemarkLogo);
}

if (ymaps) {
  ymaps.ready(initInfrastructureMap);
}
