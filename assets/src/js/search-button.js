const searchButton = document.querySelector('.js-search-button');
const searchField = document.querySelector('.js-search-field');
const searchCancel = document.querySelector('.js-button-cancel');

if (searchButton) {
  searchButton.addEventListener('click', () => {
    searchButton.classList.toggle('active');
    searchField.classList.add('active');
  });

  searchCancel.addEventListener('click', () => {
    searchCancel.classList.add('active');
    searchField.classList.remove('active');
  });
}
