function productOptions() {
  const element = document.querySelector('.js-product-description');
  if (element) {
    const buttons = element.querySelectorAll('[data-name]');

    for (const button of buttons) {
      button.addEventListener('click', () => {
        if (button.classList.contains('active')) {
          return;
        }

        const { name } = button.dataset;
        const currentNameButtons = element.querySelectorAll(
          `[data-name=${name}]`
        );
        for (const currentNameButton of currentNameButtons) {
          currentNameButton.classList.remove('active');
        }
        button.classList.add('active');
      });
    }
  }
}

productOptions();
