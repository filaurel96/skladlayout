const word = '[a-z]+';
const entityName = `${word}(?:-${word})*`;
const pseudo = '\\&\\:.*';
const cascade = '\\s*[^&].*';
const attributeSelector = '\\[.*\\]';

module.exports = {
  extends: [
    'stylelint-config-recommended-scss',
    'stylelint-config-standard',
    'stylelint-config-rational-order',
    'stylelint-config-prettier',
  ],
  rules: {
    /** That rules are needed to make empty line between CSS blocks
     * (due to prettier kill that)
     * */
    'at-rule-empty-line-before': [
      'always',
      {
        except: ['blockless-after-same-name-blockless'],
        ignore: ['after-comment', 'first-nested'],
        severity: 'warning',
      },
    ],
    'custom-property-empty-line-before': [
      'always',
      {
        except: ['after-custom-property'],
        ignore: ['after-comment', 'first-nested', 'inside-single-line-block'],
        severity: 'warning',
      },
    ],
    'declaration-empty-line-before': [
      'always',
      {
        except: ['after-declaration'],
        ignore: ['after-comment', 'first-nested', 'inside-single-line-block'],
        severity: 'warning',
      },
    ],
    'rule-empty-line-before': [
      'always',
      {
        ignore: ['after-comment', 'first-nested'],
        severity: 'warning',
      },
    ],
    'selector-class-pattern': [
      `^${entityName}(?:__${entityName})?(?:_${entityName}){0,2}$`,
      {
        resolveNestedSelectors: true,
      },
    ],
    'selector-nested-pattern': `^(?:${cascade})|(?:${pseudo})|(?:&${attributeSelector})|(?:&_{1,2}${entityName})$`,
  },
};
